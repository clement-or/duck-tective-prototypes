extends Control

func _ready():
	var items = get_tree().get_nodes_in_group("item")
	for i in items:
		i.connect("selected", $CreateHypothesis, "_on_item_selected")

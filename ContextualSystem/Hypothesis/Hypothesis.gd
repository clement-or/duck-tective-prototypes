extends VBoxContainer

var label setget set_h,get_h

var _name = "This is an hypothesis"

func set_h(string):
	_name = string
	$Label.text = string
	
func get_h():
	return _name


func _on_delete_pressed():
	queue_free()

extends ScrollContainer

signal link_selected

onready var button = preload("res://ContextualSystem/LinkView/TemplateButton.tscn")

func _ready():
	print("Ready")
	filter(ItemType.ItemType.CHARACTER)

func filter(filter):
	print("Filtering")
	
	for child in $VBoxContainer.get_children():
		child.queue_free()

	for link in Game.links:
		print("Filtering " + link.name)
		if link.before == filter:
			print("Adding " + link.name)
			var new = button.instance()
			new.text = link.name
			new.text += " " + ItemType.type_to_string(link.after)
			$VBoxContainer.add_child(new)
			new.link = link
			new.connect("selected", self, "_on_link_selected")


func _on_link_selected(link):
	visible = false
	emit_signal("link_selected", link)

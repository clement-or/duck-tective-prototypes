extends Button

signal selected

var link : Dictionary

func _on_pressed():
	emit_signal("selected", link)

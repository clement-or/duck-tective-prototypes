extends Button

signal selected

export(ItemType.ItemType) var type
export(String) var label
export(Texture) var image

onready var default_img = preload("res://Assets/Clues/Unknown.png")

func _ready():
	$Label.text = label
	if image:
		$TextureRect.texture = image
	else:
		$TextureRect.texture = default_img

func _on_pressed():
	emit_signal("selected", type, $Label.text)

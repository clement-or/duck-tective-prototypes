extends ScrollContainer

onready var item_containers = [
		$Panel/VBoxContainer/Characters/GridContainer,
		$Panel/VBoxContainer/Clues/GridContainer,
		$Panel/VBoxContainer/Places/GridContainer
	]
onready var Item = preload("res://ContextualSystem/Item.tscn")

func _ready():
	show_all()

func flush():
	for i in get_items():
		i.queue_free()

func show_all():
	flush()
	for i in Game.items:
		if i.get("used"): continue
		match i.type: 
			ItemType.ItemType.CHARACTER:
				$Panel/VBoxContainer/Characters/GridContainer.add_child(
					new_item(i.name, i.type, i.get("image"))
				)
			ItemType.ItemType.CLUE:
				$Panel/VBoxContainer/Clues/GridContainer.add_child(
					new_item(i.name, i.type, i.get("image"))
				)
			ItemType.ItemType.PLACE:
				$Panel/VBoxContainer/Places/GridContainer.add_child(
					new_item(i.name, i.type, i.get("image"))
				)

func new_item(name, type, image = null):
	var inst = Item.instance()
	inst.label = name
	inst.type = type
	inst.image = image
	return inst

func filter_by_type(type):
	pass

func filter_by_hypothesis(element, link):
	show_all()
	var items = get_items()
	#for i in items:
	#	i.visible = false
	for i in items:
		var hypothesis : String = element.name + " " + link.name + " " + i.label
		hypothesis = hypothesis.to_lower()
		print(hypothesis)
		if Game.possible_hypothesis.find(hypothesis) != -1:
			i.visible = true
			print("Set hypothesis visible")

func get_items():
	var arr = []
	for c in item_containers:
		arr.append_array(c.get_children())
	return arr

extends Node


func decapitalize(string : String):
	var first_char = string.left(1)
	var rest = string.substr(1)
	
	return first_char.to_lower() + rest

extends Node

var items = [
	{
		"name": "The canari",
		"type": ItemType.ItemType.CHARACTER,
	},
	
	{
		"name": "The unknown suspect",
		"type": ItemType.ItemType.CHARACTER
	},
	
	{
		"name": "Half-eaten steak",
		"type": ItemType.ItemType.CLUE
	},
	
	{
		"name": "Broken window",
		"type": ItemType.ItemType.CLUE
	}
]


var links = [
	{
		"before": ItemType.ItemType.CHARACTER,
		"name": "has attacked",
		"after": ItemType.ItemType.CHARACTER
	},
	
	{
		"before": ItemType.ItemType.CHARACTER,
		"name": "has broken",
		"after": ItemType.ItemType.CLUE
	},
]



var possible_hypothesis : Array = [
	"The unknown suspect has nibbled Half eaten steak",
	"The canari flew away through Broken window",
	"The unknown suspect has attacked The canari",
	"The unknown suspect has broken Broken window",
	"The canari has broken Broken window",
	"The canari has nibbled Half eaten steak",
	"The canari flew away through Broken bird cage",
	"The canari has broken Broken bird cage",
	"The canari has helped The unknown suspect",
	"The unknown suspect has helped The canari",
	"The canari has attacked The unknown suspect",
	"The unknown suspect has broken	Broken bird cage"
]

func _ready():
	var arr = []
	for h in possible_hypothesis:
		arr.push_front(h.to_lower())
		possible_hypothesis = arr

func validate(hypothesis):
	hypothesis = hypothesis.to_lower()
	print(hypothesis)
	if Game.possible_hypothesis.find(hypothesis) != -1:
		return true
	return false

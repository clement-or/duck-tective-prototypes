extends Node

enum ItemType {
	CHARACTER,
	CLUE,
	PLACE,
	TESTIMONY
}



func type_to_string(type):
	match type:
		ItemType.CHARACTER:
			return "someone"
		ItemType.CLUE:
			return "something"
		ItemType.PLACE:
			return "somewhere"
	return ""

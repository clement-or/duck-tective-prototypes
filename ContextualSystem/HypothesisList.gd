extends VBoxContainer

onready var hypothesis_node = preload("res://ContextualSystem/Hypothesis/Hypothesis.tscn")

func _ready():
	pass

func _on_CreateHypothesis_hypothesis_created(hypothesis):
	var inst = hypothesis_node.instance()
	inst.label = hypothesis
	add_child(inst)

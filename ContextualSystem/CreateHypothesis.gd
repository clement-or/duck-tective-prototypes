extends WindowDialog

signal hypothesis_created(hypothesis)

onready var link_view = $HBoxContainer/LinkView

var complete = false

var element

func _on_item_selected(type, name):
	if not visible:
		link_view.filter(type)
		$HBoxContainer/Label.text = name
		element = {
			"type": type,
			"name": name
		}
		popup()
	else:
		$HBoxContainer/Label.text += " " + Utils.decapitalize(name) + "."
		$HBoxContainer/FilteredDataView.visible = false
		complete = true
		$Button.disabled = false

func _on_create_pressed():
	if not complete: return
	emit_signal("hypothesis_created", $HBoxContainer/Label.text)
	visible = false


func _on_link_selected(link):
	$HBoxContainer/FilteredDataView.visible = true
	$HBoxContainer/Label.text += " " + link.name
	$HBoxContainer/FilteredDataView.filter_by_hypothesis(element, link)


func _on_cancel_pressed():
	visible = false


func _on_popup_about_to_show():
	$Label.text = ""
	link_view.visible = true
	$HBoxContainer/FilteredDataView.visible = false
	$Button.disabled = true
